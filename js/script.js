
var link = document.location.href;
var token = link.split("access_token=")[1];

if (token == undefined) {
  $(location).attr('href', "https://api.instagram.com/oauth/authorize/?client_id=085b1894900444cb887a0c5795b7382f&redirect_uri=http://localhost:3000&response_type=token");
} 

var instagram_data;

function getImages() {
  $.get("https://api.instagram.com/v1/users/self/media/recent/?access_token=" + token, displayData);
}

function displayData(response) {
  console.log(response);

  instagram_data = response.data;

  for (i = 0; i < instagram_data.length; i++){
    var d = instagram_data[i];
    var imgSrc = d.images.thumbnail.url; 

    var imgElement = $('<img data-index="' + i + '" class="w-100 p-1" src="' + imgSrc + '">');
    imgElement.click(imgClick);
    $("#img" + (i % 3)).append(imgElement);
  }

  $(".title").append('<img class="profile-img" id="title-img" src="' + instagram_data[0].user.profile_picture + '">');
  $(".title").append('<div id="title-user">' + instagram_data[0].user.username + '</div');
}


function imgClick(e)
{
  var index = parseInt($(e.target).attr("data-index"));
  enableFullScreen(index);
}

// FullScreen Image Function

function fullImage(data) {
  var image = data.images.standard_resolution;
  $("#full-image").html('<img id="imgFull" src="' + image.url + '" width="' + image.width + '" height="' + image.height + '">');
}

function username(data) {
  $("#user-header").html('<img class="profile-img" id="fs-img" src="' + data.user.profile_picture + '">' + '<div id="fs-username">' + data.user.username + '</div>');
}

function caption(data) {
  $("#caption").html('<div id="caption-style">' + data.caption.text + '</div>');
}

function getComments(data) {
  var mediaId = data.id;
  $.get("https://api.instagram.com/v1/media/" + mediaId + "/comments?access_token=" + token, displayComments);
}

function displayComments(comments) {
  // console.log(comments);

  for (i = 0; i < comments.data.length; i++){
    var commentData = comments.data[i]; 
    var displayComment = commentData.text;
    var commentSender = commentData.from.username;

    $("#commentSection").append('<div class="comment" id="commentSender">' + commentSender + '<div>');
    $("#commentSection").append('<div class="comment" id="commentStyling">' + displayComment + '</div>');
  }
}

$("#add-comment-btn").click(function(e) {
  var messageVal = $("#commentBox").val();
  console.log(messageVal);
});


function enableFullScreen(index) {
  var data = instagram_data[index];
  username(data);
  fullImage(data);
  caption(data);
  getComments(data);
  $("#fullScreen").show();
  $("#grid-gallery").hide();

  $("#previous-button").off("click");
  $("#next-button").off("click");

  $("#previous-button").click(function(e){
    enableFullScreen(index - 1);
    $("div").remove(".comment");
  });
  $("#next-button").click(function(e){
    enableFullScreen(index + 1);
    $("div").remove(".comment");
  });

  if ( index === instagram_data.length - 1) {
    $("#next-button").hide();
  } else {
    $("#next-button").show();
  };

  if (index === 0) {
    $("#previous-button").hide();
  } else {
    $("#previous-button").show();
  }
}

$(".closeButton").click(function(e){
  $("#grid-gallery").show();
  $("#fullScreen").hide();
  $("div").remove(".comment");
});



getImages();


